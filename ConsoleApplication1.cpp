﻿
#include <iostream>

#include <iostream>
#include "Vector.h"

int main() {
  
    Vector v(3.0, 4.0, 5.0);

  
    std::cout << "Vector components: (" << v.getX() << ", " << v.getY() << ", " << v.getZ() << ")\n";
    std::cout << "Length of the vector: " << v.length() << std::endl;

    return 0;
}
