

#pragma once
#include <memory>

class Vector {
private:
    double x;
    double y;
    double z;

public:
   
    Vector(double x_val, double y_val, double z_val) : x(x_val), y(y_val), z(z_val) {}

    
    double getX() const { return x; }
    double getY() const { return y; }
    double getZ() const { return z; }

   
    double length() const {
        return sqrt(x * x + y * y + z * z);
    }
};

